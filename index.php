<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>
<body>
    
    <?php

        $fd = fopen('quijote.txt','r');
        $fn = fopen('nuevo_quijote.txt','x');
        $nuevo_texto = "";

        while (($linea = fgets($fd)) !== false){
            $nueva_linea = str_replace('Sancho','Morty',$linea);
            $nuevo_texto .= $nueva_linea."\n";
        }

        fwrite($fn,$nuevo_texto);

        fclose($fd);
        fclose($fn);

    ?>

</body>
</html>
